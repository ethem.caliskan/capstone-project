<div class="container-fluid start-screen h-100">
        <h1 class="start-screen-title">Start</h1>

        <div class="tiles-area clear">
        <div class="tiles-grid tiles-group size-2 fg-white" >
        <div data-role="tile">
         <span class="icon mif-plus"></span>
         <span class="branding-bar">New Employee</span>
        </div>



       <div class="bg-green" data-role="tile">
        <span class="icon mif-list"></span>
    <span class="branding-bar">Employee List</span>
</div>

<div class="bg-orange" data-role="tile" data-size="wide">
        <span class="icon mif-equalizer"></span>
    <span class="branding-bar">Authorize Departments and Captions</span>
</div>
</div>
<div class="tiles-grid tiles-group size-2 fg-white" >
<div class="bg-indigo" data-role="tile" data-size="large">
        <span class="icon mif-chart-dots"></span>
    <span class="branding-bar">General Reports And Charts </span>
</div>


</div>

<div class="tiles-grid tiles-group size-2 fg-white" >
<div class="bg-grayBlue" data-role="tile" data-size="wide">
        <span class="icon mif-list2"></span>
    <span class="branding-bar">All Logs</span>
</div>

<div class="bg-cobalt" data-role="tile">
        <span class="icon mif-apps"></span>
    <span class="branding-bar">Settings</span>
</div>

<div class="bg-red" data-role="tile">
        <span class="icon mif-exit"></span>
    <span class="branding-bar">Log out</span>
</div>


</div>
        </div>
    </div>
